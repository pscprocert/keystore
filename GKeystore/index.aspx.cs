﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;

namespace GKeystore
{
    public partial class index1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            //Almacenamos la ruta del Java en una variable
            string JRE_Path = "C:\\Program Files (x86)\\Java\\jre1.8.0_121\\bin";

            //Almacenamos los datos obtenidos del formulario en una variable
            string dname = "\"cn=" + TextBox1.Text + ", "
                + "ou=" + TextBox2.Text + ", "
                + "o=" + TextBox3.Text + ", "
                + "l=" + TextBox4.Text + ", "
                + "s=" + TextBox5.Text + ", "
                + "c=" + TextBox6.Text + "\"";

            //Almacenamos el alias en una variable
            string alias = "user";

            //Almacenamos la ruta de salida en una variable
            string outPath = "C:\\temp\\";

            //Almacenamos la contraseña del keystore en una variable
            string pass = "123456";

            //Creamos el comando a ejecutar con las variables obtenidas
            String gk_Command = "cd " + JRE_Path + " & " +
                "keytool -genkey -alias " + alias + " -dname " + dname +
                " -keyalg RSA -keystore " + outPath + "Keystore.jks" +
                " -keysize 2048 -storepass " + pass +
                " -keypass " + pass;

            //Indicamos que deseamos inicializar el proceso cmd.exe junto a un comando de arranque. 
            //(/C, le indicamos al proceso cmd que deseamos que cuando termine la tarea asignada se cierre el proceso).
            System.Diagnostics.ProcessStartInfo procjks = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + gk_Command);

            // Indicamos que la salida del proceso se redireccione en un Stream
            procjks.RedirectStandardOutput = true;
            procjks.UseShellExecute = false;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            procjks.CreateNoWindow = false;
            //Inicializa el proceso
            System.Diagnostics.Process proc1 = new System.Diagnostics.Process();
            proc1.StartInfo = procjks;
            proc1.Start();

            //Esperamos a que el jks sea creado
            while (!System.IO.File.Exists("C:\\temp\\Keystore.jks"))
            {}

            //Realizamos el mismo procedimiento con el comando para generar la peticion
            //Creamos el comando a ejecutar con las variables correspondientes
            String csr_Command = "cd " + JRE_Path + " & " +
                "keytool -certreq -keyalg RSA -keysize 2048 -alias " + alias +
                " -file " + outPath + "Request.txt" +
                " -keystore " + outPath + "Keystore.jks" +
                " -storepass " + pass +
                " -keypass " + pass;

            System.Diagnostics.ProcessStartInfo proccsr = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + csr_Command);
            
            // Indicamos que la salida del proceso se redireccione en un Stream
            proccsr.RedirectStandardOutput = true;
            proccsr.UseShellExecute = false;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            //Inicializa el proceso
            System.Diagnostics.Process proc2 = new System.Diagnostics.Process();
            proc2.StartInfo = proccsr;
            proc2.Start();

            //Esperamos a que el csr sea creado
            while (!System.IO.File.Exists("C:\\temp\\Request.txt"))
            {}
            
            Thread.Sleep(2000);
            
            Correo send = new Correo();
            send.correoadjunto("contacto@procert.net.ve", "C:\\temp\\Request.txt", TextBox1.Text);
                }
            }
        }