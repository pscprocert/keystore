﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="GKeystore.index1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"/> 
    <style>

        *{
            padding : 0;
            margin: 0;
            box-sizing: border-box;
            font-family: 'Raleway', sans-serif;
        }

        body {
            background-image: url(OneDrive-Microsoft.jpg);
        }

        form {
            background-color: rgba(0,0,0, 0.3);
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .modal {
            width: 60%;
            height: 60%;
            background-color: rgb(7,74,178);
            border-radius: 10px 10px;
            box-shadow: black 2px 2px 3px;
            padding: 20px;
            display: flex;
            flex-flow: column nowrap;
            justify-content: space-around;
            align-items: center;
        }

        .space {
            width: 70%;
            height: 50px;
        }

        #TextBox1, #TextBox2, #TextBox3, #TextBox4, #TextBox5, #TextBox6 {
            width: 100%;
            height: 100%;
            border-radius: 10px 10px;
            border: none;
            font-size: 24px;
            color: rgba(0,0,0,0.7);
            text-align: center;
        }

        #Button1 {
            width: 70%;
            height: 50px;
            background-color: rgb(7,74,178);
            color: white;
            border: solid 3px white;
            font-size: 24px;
            font-weight: bold;
        }

        #Label1 {
            font-size: 5em;
            text-align: center;
            padding: 20px;
            background-color: white;
            width: 98.5vw;
            position: absolute;
            bottom: -150px;
        }
    </style>
    <title>G-Keystore</title>
</head>
<body>
    <form id="form1" runat="server">

        <div class="modal">
        
        <div class="space">
            <asp:TextBox ID="TextBox1" runat="server" pattern="[A-Za-z ]+" title="Este campo solo admite letras e.j. Bill Gates" placeholder="Nombre y Apellido" required="true" MaxLength="60"></asp:TextBox>
        </div>

        <div class="space">
            <asp:TextBox ID="TextBox2" runat="server" title="Solo letras e.j. Microsoft" placeholder="Organización" required="true" MaxLength="60"></asp:TextBox>
        </div>

        <div class="space">
            <asp:TextBox ID="TextBox3" runat="server" title="Solo letras e.j. Departamento de Desarrollo" placeholder="Unidad de Organización" required="true" MaxLength="60"></asp:TextBox>
        </div>

        <div class="space">
            <asp:TextBox ID="TextBox4" runat="server" title="Solo letras e.j. Chacao" placeholder="Localidad" required="true" MaxLength="60"></asp:TextBox>
        </div>

        <div class="space">
            <asp:TextBox ID="TextBox5" runat="server" pattern="[A-Za-z ]+" title="Solo letras e.j. Miranda" placeholder="Estado" required="true" MaxLength="60"></asp:TextBox>
        </div>

        <div class="space">
            <asp:TextBox ID="TextBox6" runat="server" pattern="[A-Z]{2}" title="Este campo solo admite 2 letras mayusculas e.j. VE" placeholder="País" required="true" MaxLength="2"></asp:TextBox>
        </div>

        <asp:Button ID="Button1" runat="server" Text="Generar" OnClick="Button1_Click" />

            </div>

    </form>
</body>
</html>
