﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GKeystore
{
    public partial class Install : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Almacenamos la ruta del Java en una variable
            string JRE_Path = "C:\\Program Files (x86)\\Java\\jre1.8.0_121\\bin";

            //Almacenamos el alias en una variable
            string alias = "user";

            //Almacenamos la ruta de salida en una variable
            string outPath = "C:\\temp\\";

            //Almacenamos la ruta de los certificados raices en variables
            string suscerte = "-alias root -file C:\\temp\\Certificates\\Suscerte.pem";
            string procert = "-alias sub -file C:\\temp\\Certificates\\PSCProcert.pem";
            string user = "-alias user -file C:\\temp\\User.pem";

            //Almacenamos la contraseña del keystore en una variable
            string pass = "123456";

            //Declaramos la variable cadena
            string cadena = null; ;

            for (int i = 0; i < 2; i++) {

                if (i == 0) {
                    cadena = suscerte;
                }

                if (i == 1)
                {
                    cadena = procert;
                }

                if (i == 2) {
                    cadena = user;
                }

            //Creamos los comandos a ejecutar con las variables obtenidas
            String ir_Command = "cd " + JRE_Path + " & " +
                "keytool -import -trustcacerts " + cadena +
                " -keystore \""+outPath+"Keystore.jks"+"\" -noprompt " +
                " -storepass " + pass +
                " -keypass " +pass;

            //Indicamos que deseamos inicializar el proceso cmd.exe junto a un comando de arranque. 
            //(/C, le indicamos al proceso cmd que deseamos que cuando termine la tarea asignada se cierre el proceso).
            System.Diagnostics.ProcessStartInfo procjks = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + ir_Command);

            // Indicamos que la salida del proceso se redireccione en un Stream
            procjks.RedirectStandardOutput = true;
            procjks.UseShellExecute = false;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            procjks.CreateNoWindow = false;
            //Inicializa el proceso
            System.Diagnostics.Process proc1 = new System.Diagnostics.Process();
            proc1.StartInfo = procjks;
            proc1.Start();

            Label1.Text = ir_Command;
            }
        }
    }
}