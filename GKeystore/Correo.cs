﻿#region Referencias
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Xml.Linq;
using System.Net.Mime;
using System.IO;
#endregion

public class Correo
{
    #region Variables Generales
    //Datos de conexion al servidor SMTP
    string us = "AKIAIVHGWQJR3FXSWHXA";
    string pass = "AjPFCC30XlK80P+Af38a2SC3ceINP1RoYnnX00/VMuvx";
    string host = "email-smtp.us-west-2.amazonaws.com";
    int port = 587;
    
    //Remitente y Copia
    string remitente = "info@procert.net.ve";
    string subject = "No Answer";
    #endregion

    #region Envio de correo
    protected void envia(MailMessage msg)
    {
        //Conexion al Servidor SMTP 
        SmtpClient client = new SmtpClient();
        client.Credentials = new System.Net.NetworkCredential(us, pass);
        client.Port = port;
        client.Host = host;
        client.EnableSsl = true;

        try
        { client.Send(msg); }
        catch (System.Net.Mail.SmtpException ex)
        { System.Web.HttpContext.Current.Response.Write("<script language=javascript>alert('ERROR''" + ex.Message + "');</script>"); }
    }
    #endregion

    #region Correo de procesos con adjunto
    public void correoadjunto(string correo, string adjunto, string usuario)
    {
        MemoryStream adjuntopdff = new MemoryStream(File.ReadAllBytes(adjunto));

        //Mensaje en texto plano
        string mensaje = ""+ usuario+" acaba de generar un almacen de claves, adjunto al correo se encuentra la peticion, Por favor contactar a la persona para realizar los procesos de validación";
        AlternateView plainView = AlternateView.CreateAlternateViewFromString(mensaje, Encoding.UTF8, MediaTypeNames.Text.Plain);

        //Mensaje en HTML
        string mensaje_html = "<h4>"+ usuario +"  acaba de generar un almacen de claves, adjunto al correo se encuentra la peticion<br><br>Por favor contactar a la persona para realizar los procesos de validación</h4>";
        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(mensaje_html, Encoding.UTF8, MediaTypeNames.Text.Html);

        //Proceso para envio de correo
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        msg.To.Add("" + correo);
        msg.From = new MailAddress(remitente, subject, System.Text.Encoding.UTF8);
        msg.Subject = "Notificación de Generacion de Keystore en One Drive";
        msg.SubjectEncoding = System.Text.Encoding.UTF8;
        msg.BodyEncoding = System.Text.Encoding.UTF8;
        msg.IsBodyHtml = true;
        msg.AlternateViews.Add(plainView);
        msg.AlternateViews.Add(htmlView);
        msg.Attachments.Add(new Attachment(adjuntopdff, adjunto));

        envia(msg);
        adjuntopdff.Close();
    }
    #endregion
}